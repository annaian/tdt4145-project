import sqlite3
import os.path
from controller import coffeeControl, login
from userStories.story1 import runStory1
from userStories.story2 import runStory2
from userStories.story3 import runStory3
from userStories.story4 import runStory4
from userStories.story5 import runStory5


def createView(cursor):
    '''Creates the view CoffeeAndRoastery if it does not exist in the database
    :param cursor: Cursor for interacting with the database
    '''
    createViewCoffeeAndRoastery = """
        CREATE VIEW IF NOT EXISTS CoffeeAndRoastery(CoffeeID, CoffeeName, Price, RoasteryName, RoasteryID, BatchID) 
        AS SELECT RC.CoffeeID, RC.CoffeeName, RC.Price, R.RoasteryName, R.RoasteryID, RC.BatchID
        FROM RoastedCoffee AS RC INNER JOIN RoastedBy AS RB ON RC.CoffeeID = RB.CoffeeID
        INNER JOIN Roastery AS R ON R.RoasteryID = RB.RoasteryID
        ORDER BY RC.CoffeeID;
    """
    try:
        cursor.executescript(createViewCoffeeAndRoastery)
    except:
        print("Something went wrong when creating view of coffees and roasteries.","Please restart the program")


def chooseStory(user, db):
    '''Prints options for user stories and asks the user for input to choose a story
    :param user: User object
    :param db: CoffeeControl object
    '''

    print("\nChoose the user story you want to execute.\n",
          "Write the number corresponding to your chosen user story and press enter.\n",
          "Overview of user stories:\n",
          "1: Post a coffee review\n",
          "2: View a list of the users that have tasted the most unique coffees this year\n",
          "3: View the coffees that gives the most value per money (highest score per price)\n",
          "4: Search for coffees containing the word 'floral'\n",
          "5: Search for coffees that are from Rwanda or Colombia that are not washed\n",
          "Input an empty string to end the program\n"
          )

    cursor = db.cursor
    story = input("Selection: ")
    if story == "1":
        runStory1(db, user)
    elif story == "2":
        runStory2(cursor)
    elif story == "3":
        runStory3(cursor)
    elif story == "4":
        runStory4(cursor)
    elif story == "5":
        runStory5(cursor)
    elif not story:
        print("Program ended by user")
        db.closeCon()
    else:
        print("Invalid input. Try again")
    if story:
        chooseStory(user, db)


def main():
    '''Initiates the program with database.db. Creates a database called database.db if 
    it does not exist yet.
    '''
    filename = 'database.db'

    if os.path.exists(filename):
        print('Database exists, do not execute sql\n')
    else:
        print('Database does not exist, executing sql script\n')
        con = sqlite3.connect(filename)
        with open('CoffeeDB.sql', 'r') as sql_file:
            con.executescript(sql_file.read())
        con.close()

    db = coffeeControl(filename)
    createView(db.cursor)
    user = login(db)
    print("Log in successful ✅")
    print("Welcome back,", user.getUserName(), "👋")

    chooseStory(user, db)


main()
