from datetime import date


def checkIfRoastedBy(coffeeName, roastery, cursor):
    """Checks if the coffee is roasted by the given roastery
    :param coffeeName: Name of coffee
    :param roastery: Name of roastery
    :param cursor: Relevant cursor object with a connection to the database
    :return: CoffeeID if a match is found, 0 if not
    """

    # Query that matches coffee with roastery if it exists
    # Uses view created in former query
    try:
        # If, for some reason, the view from the former step isn't created, this function creates a new view.
        # Should not be executed if program runs normally. 

        query = """
        SELECT CoffeeID, RoasteryID
        FROM CoffeeAndRoastery
        WHERE CoffeeName LIKE ? and RoasteryName like ?
        """
        cursor.execute(query, (coffeeName, roastery))
        rows = cursor.fetchone()

        # If match found in the database, return coffeeID. Else return 0.
        if rows is None:
            print("No match was found from the given CoffeeName and RoasteryName")
            return 0
        else:
            print("Match found!")

            # print("Returning following CoffeeID: ", rows[0])
            return rows[0]

    except Exception as e:
        print("Exception was called! returned 0 from checkIfRoastedBy", str(e))
        return 0


def runStory1(db, user):
    '''Runs user story 1: Asks the user to post a coffee tasting and posts it
    :param db: Relevant database object
    :param user: User object for the person posting
    '''
    # Get today's date as string
    today = str(date.today())
    
    # Post the tasting
    db.postTasting(user, today)
