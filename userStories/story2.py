from prettytable import from_db_cursor


def runStory2(cursor):
    '''Runs user story 2: Prints a list of the users who have tasted most unique coffees
    :param cursor: Relevant cursor object to the database
    '''
    queryUserWithTastingCounts = """
    SELECT User.UserID, COUNT(DISTINCT CoffeeID) AS DistinctCoffeesConsumed 
    FROM User INNER JOIN CoffeeTasting AS CT ON User.UserID=CT.UserID
    GROUP BY User.UserID
    """

    userstory2 = """
    SELECT U.FullName, DistinctCoffeesConsumed
    FROM User AS U INNER JOIN ("""+ queryUserWithTastingCounts+""") USING (UserID)
    ORDER BY DistinctCoffeesConsumed DESC
    """
    
    try:
        cursor.execute(userstory2)
        table = from_db_cursor(cursor)
        print("Users with most unique coffees tasted: ")
        print(table)

    except Exception as e:
        print("Something went wrong in userstory 2", str(e))