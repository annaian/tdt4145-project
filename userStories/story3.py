from prettytable import from_db_cursor


def runStory3(cursor):
    """Runs user story 3: Prints Coffees with the highest value for money
    :param cursor: A cursor object with a connection to the database
    """

    userstory3 = """
    SELECT CR.ROASTERYNAME, CR.COFFEENAME, CR.PRICE, AvgScore
    FROM 
    (SELECT CT.CoffeeID, AVG(CT.SCORE) as AvgScore
        FROM CoffeeTasting AS CT
        GROUP BY CoffeeID) 
    INNER JOIN CoffeeAndRoastery AS CR USING (CoffeeID)
    ORDER BY (AvgScore/CR.PRICE) DESC
    """

    try:
        cursor.execute(userstory3)
        print("Coffees with the highest value for money: ")
        table = from_db_cursor(cursor)
        print(table)


    except Exception as e:
        print ("Error in user story 3: ", str(e))

