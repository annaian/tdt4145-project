from prettytable import from_db_cursor

def runStory5(cursor):
    '''Runs user story 5: Prints a list of coffees that are not washed and are from
    Colombia or Rwanda
    :param cursor: Relevant cursor object to the database
    '''
    queryPNxBatch = """
        SELECT PM.ProcessingName, FarmID, Batch.BatchID
        FROM ProcessingMethod AS PM INNER JOIN Batch on PM.ProcessingName=Batch.ProcessingName
        WHERE PM.ProcessingName NOT LIKE 'washed%'
        """

    queryFarmWithLocation = """
        SELECT Farm.FarmID
        FROM Location AS L INNER JOIN Farm on L.LocationID = Farm.LocationID
        WHERE (Country = 'Rwanda') OR (Country= 'Colombia')
        """

    queryUserStory5 = """
        SELECT CoffeeName, RoasteryName 
        FROM ((""" + queryPNxBatch + """) INNER JOIN CoffeeAndRoastery USING(BatchID))
        INNER JOIN (""" + queryFarmWithLocation + """) USING (FarmID)
        """
    try:
        cursor.execute(queryUserStory5)
        print("Coffees from Rwanda or Colombia that are not washed:")
        table = from_db_cursor(cursor)
        print(table)

    except Exception as e:
        print("Error in user story 5: ", str(e))
