from prettytable import from_db_cursor

def runStory4(cursor):
    '''Runs user story 4: Prints a list coffees that have been described as "floral"
    :param cursor: Relevant cursor object to the database
    '''

    coffeeAndTasting = """
    SELECT DISTINCT RC.CoffeeName, RC.CoffeeID
        FROM RoastedCoffee as RC LEFT JOIN CoffeeTasting AS CT USING (CoffeeID)
        WHERE (CT.TastingNotes LIKE '%floral%') OR (RC.Description LIKE '%floral%')
    """

    queryUserStory4 = """
        SELECT CR.CoffeeName, CR.RoasteryName 
        FROM ("""+coffeeAndTasting+""") INNER JOIN CoffeeAndRoastery AS CR USING (CoffeeID)"""
    try:
        cursor.execute(queryUserStory4)
        print("Coffees that are described as floral:")
        table = from_db_cursor(cursor)
        print(table)

    except Exception as e:
        print ("Error in user story 4: ", str(e))