# Project Part 2

## Classes 

### CoffeeController: Handles database interactions and insertions
Contains cursors and connectors to interact with a database. It also contains functions for input-based insertions into the database. CoffeeDB is useful because it gives easy access to con and cursor for interacting with the database, which are frequently used, and the function addIfNotThere() enables quick insertions of data like coffee tastings and new users.

### User: Handles user information
Contains user information. User is useful because we often refer to a specific user, and the class makes it easier to store this information effectively. For instance, when a user posts a tasting, getUserID() is called to get the correct userID in the insertion.


## File system
**Main:** Contains code for running user stories 1-5 and for the user to choose which story to execute from input. This file also runs the SQL script to generate the database if it is not already created.

**Controller:** Contains the classes and functions for controlling and interacting with the system. This involves the classes CoffeeController and User, and the functions checkIfUserExists() and login(). 

**UserStories:** A folder that contains one file for each user story. These stories are imported into main.

**CoffeeDB:** An SQL script that is executed when the program is started. The script generates the database and fills the table with data that is shown in the user stories. 


## User Stories

### User story 1

When running the main() function in main.py, a connection is created to the database through the class CoffeeController. If the database file does not exist, the program will execute the SQL script CoffeeDB.sqmergel to create the database. Additionally, a view called CoffeeAndRoastery is created if it does not already exist. The query consists of joining tables RoastedCoffee, RoastedBy, and Roastery using inner join, first on CoffeeID, then on RoasteryID. The columns CoffeeID, CoffeeName, Price, RoasteryName, BatchName and RoasteryID are selected for the new view called ‘CoffeeAndRoastery’.  A view is used here as a table with coffees and roasteries is frequently used in other queries. Additionally, using a view breaks down complexity in nested queries. As views get updated whenever there is a change in the database, there is no need to drop the view.

The user is then asked to log in. If the user inputs invalid login info, it can type “r” to register a new user or press enter to continue. After inputting the correct login details, the function chooseStory() is executed. The user may then input the value “1”, which initiates the function runStory1() in the file story1.py. runStory1() calls on the function postTasting() which is in the separate file controller.py.

1. First, the program prints a table containing names and roasteries of all coffees in the database. This action is done through the function showCoffeesAndRoasteries() which utilizes the view CoffeeAndRoastery. The columns CoffeeName and RoasteryName are selected and printed to the user, to help the user write the correct names. 

2. Next, the user is asked to input the name of the coffee being reviewed, followed by the name of the roastery of the coffee. To check if the values CoffeeName and RoasteryName exist in the database and that they have a valid relation to one another, the function checkIfRoastedBy() in story1.py is called.This function utilizes the view CoffeeAndRoastery created in the former step and selects tuples with the same CoffeeName and RoasteryName as the user’s input. fetchOne() is then called on the cursor and saved in a new variable. If there are no tuples in this  variable, no relation exists between the user’s inputted CoffeeName and RoasteryName. If this is the case, the user has to input a set of new values. On the other hand, if there exists a tuple in the resulting table,  the coffeeID is returned to the postTasting() function and “Match found!” is printed to the console.
. 
3. Thirdly, the user is asked to input the score it wishes to give the coffee. If the input score is not an integer between 0 and 10, the user is asked to retype the score input. The user is then asked to input the description for the coffee tasting post. 

4. Lastly, all values needed to insert a new coffee tasting post have been successfully collected. The code has also checked that the inputted values are within the correct domain of the data types of the various attributes. The function postTasting() then executes an SQL-INSERT of these values, as well as the user's id and today’s date. The function ends with the connection commiting the results to the database. 


### User story 2

When logged in, the user is presented with different options. By inputting “2”, a query is sent to the database through the function runStory2(). The query is nested, where the inner selection extracts a table with userIDs and distinct coffees consumed by each userID. As we need the users’ names, this table is then joined with another user table where the columns with users’ names and coffees consumed are selected and ordered by coffees consumed (descending). 

Finally, the selected table with columns FullName and DistinctCoffeesConsumed is printed to the user. 


### User story 3

After logging in, the user can input “3” in main to go to user story 3. In userStories/story3.py, through runStory3(), an SQL query is combined. 

1. First, a nested query is sent to the database, where the innermost query selects CoffeeID and the aggregate function AVG on Score. This query groups the resulting table by CoffeeID. 
2. Next, the view CoffeeAndRoastery is utilized. The selection above is combined with CoffeeAndRoastery using an inner join on CoffeeID. RoasteryName, CoffeeName, Price and AvgScore (average score) is then printed to the console. This table is ordered by AvgScore divided by price. 


### User story 4
If the user inputs value ‘4’ in the menu, the function runStory4() is executed. This function sends a nested query to the database 

1. The first selection combines tables RoastedCoffee and CoffeeTasting with a LEFT JOIN. A Left join is used here to make sure that coffees that have not received any reviews will be added to the resulting table. The table is then filtered to coffees with  descriptions that include the word ‘floral’ or tasting notes that include ‘floral’. Next, the tables CoffeeName and CoffeeID are selected with a DISTINCT statement to delete copies of coffees that appear in both CoffeeTasting and RoastedCoffee.

2. Next, the selections above are inner joined with the view CoffeeAndRoastery on CoffeeID. The columns CoffeeName and RoasteryName are selected. 

After the query has been sent through the cursor object, a table is created and printed to the console. The user is then able to see all coffees that are described as ‘floral’ by either users or roasteries. 


### User story 5
In the final story, the user asks to see coffees from Rwanda or Colombia that are not washed. If option 5 in the menu is chosen, a nested query is sent to the database through the function runStory5(). 

1. One selection consists of joining tables ProcessingMethod and Batch and filtering out ProcessingName that start with ‘washed’. The columns ProcessingName, FarmID and BatchID are selected.
2. Secondly, the selection above is combined with the view CoffeeAndRoastery using an inner join on BatchID.
3. Lastly, the selections above are joined with the tables Farm and Location joined on LocationID with a WHERE clause that selects farms with a location in Country=’Rwanda’ or Country=’Colombia. This nested selection is combined with the selection in step 2 using FarmID. The columns CoffeeName and RoasteryName are selected and printed to the console.

Thus, the user is able to view coffees with their respective roastery that are made with unwashed beans in Rwanda or Colombia.


## Assumptions

- It is required that the user logs in to access the application
- All processing methods that involve washing beans start with ‘washed’. 
- Only coffees that are in the system can be reviewed and it is not desirable to let users input their own coffees. 
- The coffee with corresponding data has been inserted for use case 1.
- For user story 4 and 5, there is no need to enable the user to search for other countries or search words.
- It is assumed that ‘natural Bourbon’ is the name of an unprocessed bean, and that the processing method is unrelated.
- Users are only able to insert information to the User and CoffeeTastings tables.