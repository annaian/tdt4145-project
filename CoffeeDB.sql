CREATE TABLE User (
   UserID	INTEGER NOT NULL,
   Email	TEXT,
   Password VARCHAR(30),
   FullName	VARCHAR(30),
   CONSTRAINT User_PK PRIMARY KEY (UserID));

CREATE TABLE CoffeeTasting (
   PostID    INTEGER NOT NULL,
   TastingNotes	TINYTEXT,
   Score     TINYINT(10),
   TastingDate     DATE,
   CoffeeID	INTEGER NOT NULL, 
   UserID	INTEGER NOT NULL,
   CONSTRAINT CoffeeTasting_PK PRIMARY KEY (PostID),
   CONSTRAINT CoffeeTasting_FK1 FOREIGN KEY (CoffeeID) REFERENCES RoastedCoffee(CoffeeID)
      ON UPDATE CASCADE --Assume that RoastedCoffees are rarely deleted
	  ON DELETE NO ACTION, --For user statistics and history, the tasting should remain when a coffee is deleted
   CONSTRAINT CoffeeTasting_FK2 FOREIGN KEY (UserID) REFERENCES User(UserID)
      ON UPDATE CASCADE
      ON DELETE CASCADE); --To maintain user privacy, tastings should be deleted if a user is deleted

CREATE TABLE RoastedCoffee (
   CoffeeID    INTEGER NOT NULL,
   CoffeeName	VARCHAR(60), --Assume that names does not exceed 60 signs
   Description     TINYTEXT, --Assume that the description of a RoastedCoffee does not exceed 255 characters
   Price	DOUBLE(8, 2), --Assume that price does not exceed 999 999.99
   BatchID	INTEGER NOT NULL,
   CONSTRAINT RoastedCoffee_PK PRIMARY KEY (CoffeeID),
   CONSTRAINT RoastedCoffee_FK FOREIGN KEY (BatchID) REFERENCES Batch(BatchID)
      ON UPDATE CASCADE 
      ON DELETE NO ACTION);
	  
CREATE TABLE Roastery (
   RoasteryID INTEGER NOT NULL,
   RoasteryName	VARCHAR(40),
   CONSTRAINT Roastery_PK PRIMARY KEY (RoasteryID));
	  
CREATE TABLE RoastedBy (
   CoffeeID    INTEGER NOT NULL,
   RoasteryID  INTEGER NOT NULL,
   RoastDate	DATE,
   RoastDegree VARCHAR(6), --Only 6 as it is either light, medium or dark. Used instead of ENUM as it is not availible in SQLite
   CONSTRAINT RoasteBy_PK PRIMARY KEY (CoffeeID),
   CONSTRAINT RoastedBy_FK1 FOREIGN KEY (CoffeeID) REFERENCES RoastedCoffee(CoffeeID)
	  ON UPDATE CASCADE
      ON DELETE CASCADE,
	CONSTRAINT RoastedBy_FK2 FOREIGN KEY (RoasteryID) REFERENCES Roastery(RoasteryID)
	  ON UPDATE CASCADE
      ON DELETE CASCADE);
	  
CREATE TABLE ProcessingMethod (
	ProcessingName VARCHAR(40) NOT NULL,
	Description TEXT,
	CONSTRAINT ProcessingMethod_PK PRIMARY KEY (ProcessingName));
	
CREATE TABLE Farm (
	FarmID    INTEGER NOT NULL,
	FarmName VARCHAR(40),
	LocationID INTEGER NOT NULL,
	MetersAboveSeaLevel DOUBLE(6, 2), --Assume number of meters does not exceed 9 999.99
	CONSTRAINT Farm_PK PRIMARY KEY (FarmID),
	CONSTRAINT Farm_FK1 FOREIGN KEY (LocationID) REFERENCES Location(LocationID)
	  ON UPDATE CASCADE
      ON DELETE NO ACTION);
	  
CREATE TABLE Location (
	LocationID INTEGER NOT NULL,
	Country TINYTEXT,
	Region TINYTEXT,
	CONSTRAINT Location_PK PRIMARY KEY (LocationID));

CREATE TABLE Batch (
   BatchID    INTEGER NOT NULL,
   HarvestYear	YEAR,
   PricePaidPerKilo DOUBLE(8, 2), --Assume price does not exceed 999 999.99
   ProcessingName VARCHAR(40) NOT NULL,
   FarmID INTEGER NOT NULL,
   CONSTRAINT Batch_PK PRIMARY KEY (BatchID),
   CONSTRAINT Batch_FK1 FOREIGN KEY (ProcessingName) REFERENCES ProcessingMethod(ProcessingName)
	  ON UPDATE CASCADE
      ON DELETE NO ACTION,--Do not want to delete as we still want to know the processing name
	CONSTRAINT Batch_FK2 FOREIGN KEY (FarmID) REFERENCES Farm(FarmID)
	  ON UPDATE CASCADE
      ON DELETE NO ACTION);
	  
CREATE TABLE UnprocessedBean (
	BeanID    INTEGER NOT NULL,
	BeanName VARCHAR(60),
	Species VARCHAR(10),--Only 10 as it is either arabica, robusta or liberica. Used instead of ENUM as it is not availible in SQLite
	CONSTRAINT UnprocessedBean_PK PRIMARY KEY (BeanID));
	  
CREATE TABLE ConsistsOf (
	BatchID  INTEGER NOT NULL,
   BeanID	INTEGER NOT NULL,
   PRIMARY KEY(BatchID, BeanID),
   CONSTRAINT ConsistsOf_FK1 FOREIGN KEY (BatchID) REFERENCES Batch(BatchID)
	  ON UPDATE CASCADE
      ON DELETE CASCADE,
	CONSTRAINT ConsistsOf_FK2 FOREIGN KEY (BeanID) REFERENCES UnprocessedBean(BatchID)
	  ON UPDATE CASCADE
      ON DELETE CASCADE);

CREATE TABLE GrownBy (
	FarmID   INTEGER NOT NULL,
    BeanID	INTEGER NOT NULL,
	PRIMARY KEY(FarmID, BeanID),
   CONSTRAINT GrownBy_FK1 FOREIGN KEY (FarmID) REFERENCES Farm(FarmID)
	  ON UPDATE CASCADE
      ON DELETE CASCADE,
	CONSTRAINT GrownBy_FK2 FOREIGN KEY (BeanID) REFERENCES UnprocessedBean(BatchID)
	  ON UPDATE CASCADE
      ON DELETE CASCADE); 

/* Roastery */
INSERT INTO ROASTERY(ROASTERYNAME) VALUES ("Burning Beans");
INSERT INTO ROASTERY(ROASTERYNAME) VALUES ("Jacobsen & Svart");
INSERT INTO ROASTERY(ROASTERYNAME) VALUES ("The Burnery");
INSERT INTO ROASTERY(ROASTERYNAME) VALUES ("Powerpuffbrennerne");

/* Location */
INSERT INTO LOCATION(COUNTRY, REGION) VALUES ("Colombia", "Caldas");
INSERT INTO LOCATION(COUNTRY, REGION) VALUES ("Guatemala", "Zacapa");
INSERT INTO LOCATION(COUNTRY, REGION) VALUES ("El Salvador", "Santa Ana");
INSERT INTO LOCATION(COUNTRY, REGION) VALUES ("Rwanda", "Virunga");

/*  Unprocessed Bean */
INSERT INTO UNPROCESSEDBEAN(BEANNAME, SPECIES) VALUES ("Sumatran", "Arabica"); 
INSERT INTO UNPROCESSEDBEAN(BEANNAME, SPECIES) VALUES ("Liberacion", "Coffea robusta");
INSERT INTO UNPROCESSEDBEAN(BEANNAME, SPECIES) VALUES ("Real Columbia", "Coffea liberica");
INSERT INTO UNPROCESSEDBEAN(BEANNAME, SPECIES) VALUES ("Guatemalan Elephant", "Coffea robusta");
INSERT INTO UNPROCESSEDBEAN(BEANNAME, SPECIES) VALUES ("Natural Bourbon", "Arabica"); 

/* Processing Method */
INSERT INTO PROCESSINGMETHOD(PROCESSINGNAME, DESCRIPTION) VALUES ("Washed", "Freshly harvested coffee is sorted for ripeness and the fruit is removed within 24 hours of harvest. The cherries go then go through depulping, then fermentation will begin. Lastly, the beans are washed.");
INSERT INTO PROCESSINGMETHOD(PROCESSINGNAME, DESCRIPTION) VALUES ("Natural", "The fruit is left on the seed for the duration of the drying process. After the drying process, the coffee goes through a dry mill which removes the fruit and the parchment layer around the seed.");
INSERT INTO PROCESSINGMETHOD(PROCESSINGNAME, DESCRIPTION) VALUES ("Honey", "A process that falls between washed and natural.");

/* Farm */
INSERT INTO FARM(FARMNAME, LOCATIONID, METERSABOVESEALEVEL) VALUES ("Family farm", 1, 1020);
INSERT INTO FARM(FARMNAME, LOCATIONID, METERSABOVESEALEVEL) VALUES ("Guapa guatemala", 2, 1200);
INSERT INTO FARM(FARMNAME, LOCATIONID, METERSABOVESEALEVEL) VALUES ("Nombre de Dios", 3, 1500);
INSERT INTO FARM(FARMNAME, LOCATIONID, METERSABOVESEALEVEL) VALUES ("Rwanda National Corp.", 4, 900);
INSERT INTO FARM(FARMNAME, LOCATIONID, METERSABOVESEALEVEL) VALUES ("Colombiana coffee", 1, 1900);

/* Batch */
INSERT INTO BATCH(HARVESTYEAR, PRICEPAIDPERKILO, PROCESSINGNAME, FARMID) VALUES (2020, 8.2,'Washed', 1);
INSERT INTO BATCH(HARVESTYEAR, PRICEPAIDPERKILO, PROCESSINGNAME, FARMID) VALUES (2020, 4.5,'Natural', 2);
INSERT INTO BATCH(HARVESTYEAR, PRICEPAIDPERKILO, PROCESSINGNAME, FARMID) VALUES (2015, 12.4,'Honey', 4);
INSERT INTO BATCH(HARVESTYEAR, PRICEPAIDPERKILO, PROCESSINGNAME, FARMID) VALUES (2021, 8, 'Washed', 3);
INSERT INTO BATCH(HARVESTYEAR, PRICEPAIDPERKILO, PROCESSINGNAME, FARMID) VALUES (2021, 9.3, 'Natural', 5);

/* RoastedCoffee*/
INSERT INTO ROASTEDCOFFEE(COFFEENAME, DESCRIPTION, PRICE, BATCHID) VALUES ("Karogoto", "This Kenyan coffee from the Karogoto wet mill, has a very distinct and intense fruity flavour with a refreshing acidity. Expect winey notes of rose hips, hiciscus and blackcurrants.", 643.2, 1);
INSERT INTO ROASTEDCOFFEE(COFFEENAME, DESCRIPTION, PRICE, BATCHID) VALUES ("Caballero Geisha", "Expect expressive sweet mandarin flavours with floral notes and a herbal twist from this award winning Geisha coffee.", 420.5, 2);
INSERT INTO ROASTEDCOFFEE(COFFEENAME, DESCRIPTION, PRICE, BATCHID) VALUES ("Los Pirineos Bourbon", "This is a sweet Bourbon coffee with a rich mouthfeel and relatively low acidity. An uncomplicated coffee with notes of chocolate and roasted nuts with hints of dried fruit in the finish.", 701.2, 3);
INSERT INTO ROASTEDCOFFEE(COFFEENAME, DESCRIPTION, PRICE, BATCHID) VALUES ('Vinterkaffe 2022', 'Mild blend perfect for cold winter nights', 600, 4);
INSERT INTO ROASTEDCOFFEE(COFFEENAME, DESCRIPTION, PRICE, BATCHID) VALUES ('Sommerkaffe 2021', 'Smokey blend perfect for floral summery nights', 590, 5);

/* ConsistsOf */
INSERT INTO CONSISTSOF(BATCHID, BEANID) VALUES (1,2);
INSERT INTO CONSISTSOF(BATCHID, BEANID) VALUES (2,3);
INSERT INTO CONSISTSOF(BATCHID, BEANID) VALUES (3,1);
INSERT INTO CONSISTSOF(BATCHID, BEANID) VALUES (4,5);
INSERT INTO CONSISTSOF(BATCHID, BEANID) VALUES (5,4);

/* RoastedBy */
INSERT INTO ROASTEDBY(COFFEEID, ROASTERYID, ROASTDATE, ROASTDEGREE) VALUES (1,2, "2021-04-18", "LIGHT");
INSERT INTO ROASTEDBY(COFFEEID, ROASTERYID, ROASTDATE, ROASTDEGREE) VALUES (2,2, "2020-09-18", "MEDIUM");
INSERT INTO ROASTEDBY(COFFEEID, ROASTERYID, ROASTDATE, ROASTDEGREE) VALUES (3,4, "2022-01-18", "DARK");
INSERT INTO ROASTEDBY(COFFEEID, ROASTERYID, ROASTDATE, ROASTDEGREE) VALUES (4,2, '2022-01-20', 'LIGHT');
INSERT INTO ROASTEDBY(COFFEEID, ROASTERYID, ROASTDATE, ROASTDEGREE) VALUES (5,1, '2021-05-26', 'MEDIUM');

/* GrownBy */
INSERT INTO GROWNBY(FARMID, BEANID) VALUES (1,1);
INSERT INTO GROWNBY(FARMID, BEANID) VALUES (2,2);
INSERT INTO GROWNBY(FARMID, BEANID) VALUES (3,3);
INSERT INTO GROWNBY(FARMID, BEANID) VALUES (5,4);
INSERT INTO GROWNBY(FARMID, BEANID) VALUES (2,5);

/* User */
INSERT INTO USER(EMAIL, PASSWORD, FULLNAME) VALUES ("lailaov@stud.ntnu.no", "pw", "Laila Oftedal Voll");
INSERT INTO USER(EMAIL, PASSWORD, FULLNAME) VALUES ("annaian@stud.ntnu.no", "pw", "Anna Irene Andresen");
INSERT INTO USER(EMAIL, PASSWORD, FULLNAME) VALUES ("marcusrf@stud.ntnu.no", "pw", "Marcus Fuglestad");

/* CoffeeTasting */
INSERT INTO COFFEETASTING(TASTINGNOTES, SCORE, TASTINGDATE, COFFEEID, USERID) VALUES ("A floral and mellow coffee", 6, "2022-03-18", 1,1);
INSERT INTO COFFEETASTING(TASTINGNOTES, SCORE, TASTINGDATE, COFFEEID, USERID) VALUES ("Amazing coffee with smokey afternotes", 9, "2022-03-15", 2,1);
INSERT INTO COFFEETASTING(TASTINGNOTES, SCORE, TASTINGDATE, COFFEEID, USERID) VALUES ("Awful coffee. Not worth it", 1, "2022-03-10", 3,3);
INSERT INTO COFFEETASTING(TASTINGNOTES, SCORE, TASTINGDATE, COFFEEID, USERID) VALUES ("This coffee is the shit. Tastes like chocolate", 9, "2022-03-14", 2,2);
INSERT INTO COFFEETASTING(TASTINGNOTES, SCORE, TASTINGDATE, COFFEEID, USERID) VALUES ("OK coffee. Too mellow", 5, "2022-03-20", 1,3);
