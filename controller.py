import sqlite3

from prettytable import from_db_cursor
from userStories.story1 import checkIfRoastedBy


def removeSuffixFromEnd(s, suffix):
    '''Inputs a string s and removes the inserted suffix if the suffix is at 
    the end of the string s
    :param s: String that should loose the suffix
    :param suffix: Suffix-string that should be removed from s
    '''
    if suffix and s.endswith(suffix):
        return s[:-len(suffix)]
    return s


class User:
    '''Class that holds information on a user in the database
    '''

    def __init__(self, id, fullname, email, password):
        self.id = id
        self.fullname = fullname
        self.email = email
        self.password = password

    def __str__(self):
        return self.fullname

    def getUserId(self):
        """ 
        A function to get users' ID for insertion into CoffeeTasting
        :Return: userID
        """
        return self.id

    def getUserName(self):
        """
        :returns: Full name of user as string
        """
        return self.fullname


class coffeeControl:
    '''Class for a database object with functions for inserting data into the database.'''

    def __init__(self, filename):
        self.con = sqlite3.connect(filename)
        self.cursor = self.con.cursor()
    
    def closeCon(self):
        """
        Closes connection to db
        """
        self.con.close()

    def addIfNotThere(self, className, fieldList, inputList):
        '''Adds a tuple to the database coffeeControl if the tuple is not already in the database.
        Returns TRUE if successful and FALSE if not successful.
        '''
        try:
            # Checks if the touple is already in the database
            query = "SELECT "
            for field in fieldList:
                query += field + ", "
            query = removeSuffixFromEnd(query, ", ")
            query += " FROM " + className + " WHERE "
            for i in range(len(fieldList)):
                query += fieldList[i] + "=" + "?" + " AND "
            query = removeSuffixFromEnd(query, "AND ")
            query = removeSuffixFromEnd(query, " ")
            self.cursor.execute(query, tuple(inputList))
            print("Execute successful")
            rows = self.cursor.fetchall()
            if len(rows):
                # Prints that the combination exists if that is the case
                print("The combination already exists in the database")
                return True
            else:
                # Inserts information into the database from user input
                insertQuery = "INSERT INTO " + className + "("
                for field in fieldList:
                    insertQuery += field + ", "
                insertQuery = removeSuffixFromEnd(insertQuery, ", ")
                insertQuery += ") VALUES ("
                for i in range(len(inputList)):
                    insertQuery += "?,"
                insertQuery = removeSuffixFromEnd(insertQuery, ",")
                insertQuery += ")"
                self.cursor.execute(insertQuery, tuple(inputList))
                self.con.commit()
                responseString = className + " added to database"
                print(responseString)
                return True
        except Exception as e:
            print(e)
            return False

    def generateInputList(self, fieldList):
        '''Asks the user for inputs on all the fields in fieldList and returns a list with the inputs
        :return: A list of all inputs
        :param fieldList: A list of strings of the fields we want to create inputs for
        '''
        inputList = []
        for item in fieldList:
            answer = input(item + ": ")
            inputList.append(answer)
        return inputList

    def registerUser(self):
        '''Asks the user for input on Email, Password and Name, and creates a new user in the database
        if that combination of Email, Password and Name does not already exist. Note
        that the new user will get an automatically generated UserID.
        The code can be re-used to add other items to the database from direct input.
        '''
        fieldList = ["email", "password", "fullname"]
        inputList = self.generateInputList(fieldList)
        if not self.addIfNotThere("User", fieldList, inputList):
            print("Something was wrong with the input")
        else:
            email = inputList[0]
            password = inputList[1]
            return checkIfUserExists(email, password, self)

    def addUnprocessedBean(self):
        '''Asks the user for direct input and adds an unprocessedBean to
        the database if the combination does not already exist
        '''
        fieldList = ["BeanName", 'Species']
        inputList = self.generateInputList(fieldList)
        print(inputList)
        if not self.addIfNotThere("UnprocessedBean", fieldList, inputList):
            print("Something was wrong with the input")

    def addLocation(self):
        '''Asks the user for direct input and adds a location to
        the database if the combination does not already exist
        '''
        fieldList = ["Country", 'Region']
        inputList = self.generateInputList(fieldList)
        if not self.addIfNotThere("Location", fieldList, inputList):
            print("Something was wrong with the input")

    def postTasting(self, user, today):
        """Posts a user tasting to the database
        :param user: User object that does the tasting
        :param today: Date for today on format YYYY-MM-DD
        """
        con = self.con
        cursor = self.cursor
        showCoffeesAndRoasteries(cursor)
        coffeeid = 0
        while coffeeid == 0:
            # Takes input from user
            coffee = input("Coffee: ")
            roastery = input("Roastery: ")
            coffeeid = checkIfRoastedBy(coffee, roastery, cursor)

        tastingNote = input("Your note on the coffee: ")

        # A while loops that checks that score is an int
        validScore = 0
        while validScore == 0:
            try:
                score = int(input("Score: "))
                if isinstance(score, int) and 1 <= score <= 10:
                    validScore = 1
                else:
                    print("Invalid input. Write a number between 1 and 10")
            except ValueError:
                print("Invalid input. Write a number between 1 and 10")

        userid = user.getUserId()
        cursor.execute(
            'INSERT INTO COFFEETASTING(TASTINGNOTES, SCORE, TASTINGDATE, USERID, COFFEEID) VALUES (?,?,?,?,?)',
            (tastingNote, score, today, userid, coffeeid))

        # Drops averageScore if it exist so it is updated
        con.commit()
        print("Note posted ✅")

def showCoffeesAndRoasteries(cursor):
    """
    :param cursor: Cursor object that is connected to the database
    Prints a list of coffees in the database and their respective roastery.
    If a view of coffees and roastery does not exists, the cursor creates a view.
    """
    print("\nList of coffees and their respective roasteries in the database:")

    coffeeAndRoastery="""
    SELECT CoffeeName, RoasteryName
    From CoffeeAndRoastery
    """
    try:
        cursor.execute(coffeeAndRoastery)
        table = from_db_cursor(cursor)
        print(table)
    except Exception as e:
        print("Something went wrong when generating tables with coffee and roasteries")
        print(e)


def checkIfUserExists(email, password, db):
    """Checks if a user exists in the database by sending a query to the database.
    If the user exists, the database will return a user object. 
    :return: new Userobject, else false
    :param email: input email
    :param password: input password
    :param db: database object
    """
    cursor = db.cursor
    cursor.execute("SELECT * FROM User")
    rows = cursor.fetchall()
    for row in rows:
        id = row[0]
        dbemail = row[1]
        dbpw = row[2]
        name = row[3]
        if dbemail == email and dbpw == password:
            return User(id, name, dbemail, dbpw)
    print(
        "No user with these credentials exists. Write r if you want to register a new user, or anything else to login once more")
    answer = input('')
    if answer == "r":
        db.registerUser()


def login(db):
    """
    Creates a login prompt that tries to log a user in based on input
    :return user: The logged in user
    :param db: Relevant database object
    """
    print("Hello ☕\nPlease log in to CoffeeDB")
    login = False

    # Loop that runs until the user is logged in
    while not login:
        email = input("Write your email: ")
        pw = input("Write your password: ")
        user = checkIfUserExists(email, pw, db)

        # If the function returns a user object, end the loop
        if type(user) == User:
            return user
